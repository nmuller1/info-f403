all:
	jflex src/LexicalAnalyzer.flex
	javac -d bin -cp src/ src/Main.java
	jar cfe dist/Part3.jar Main -C bin .

testing:
		java -jar dist/Part3.jar -v -wt tree.tex test/Factorial.fs
