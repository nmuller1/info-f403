import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
	public static void main(String[] args) {
		try {
			boolean verbose = false;
			String outputFile = "";
			String inputFile = args[args.length-1];

			// checks the parameters
			int argPos = 0;
			while(argPos < args.length-1){
				if (args[argPos].equals("-v")){
					verbose = true;
				}
				else if(args[argPos].equals("-wt")){
					outputFile = args[++argPos];
				}
				argPos++;
			}
			List<Symbol> tokens = getTokens(inputFile);

			//varNames.printSymbolTable();					 Part1
			Parser parser = new Parser(tokens, verbose);
			ParseTree parseTree = parser.parse();
			if(!outputFile.isEmpty()){
				Files.write(Paths.get(outputFile), Collections.singleton(parseTree.toLaTeX()), StandardCharsets.UTF_8);
			}
			AbstractSyntaxTree abstractSyntaxTree = new AbstractSyntaxTree(parseTree);
			Files.write(Paths.get("AST.tex"), Collections.singleton(abstractSyntaxTree.toLaTeX()), StandardCharsets.UTF_8);
			Compiler compiler = new Compiler();
			System.out.println(compiler.compile(abstractSyntaxTree));
		}
		catch (Exception m){
			System.out.println(m);
			 m.printStackTrace(); 
		}
	}

	/**
	 * Reads the source file and extracts the sequence of tokens
	 *
	 * @param inputFile path to the source file
	 * @return sequence of Symbols
	 * @throws IOException
	 */
	public static List<Symbol> getTokens(String inputFile) throws IOException {
		VarNames varNames = new VarNames();
		List<Symbol> tokens = new ArrayList<>();
		Reader source = new FileReader(inputFile);
		final LexicalAnalyzer analyzer = new LexicalAnalyzer(source);
		Symbol symbol = analyzer.nextToken();
		while(symbol.getType() != LexicalUnit.EOS){
			// Store Symbols
			if (symbol.getType() == LexicalUnit.VARNAME) {
				varNames.addSymbol(symbol);
			}
			// Ignore various endline symbols
			if(!tokens.isEmpty()){
				if(!(tokens.get(tokens.size()-1).getType()==LexicalUnit.ENDLINE && symbol.getType()==LexicalUnit.ENDLINE)){
					tokens.add(symbol);
				}
			}
			else{tokens.add(symbol);}
			//System.out.println(symbol.toString());     Part1
			symbol = analyzer.nextToken();
		};
		return tokens;
	}

}