import java.io.*;
import java.util.*;

/**
 * Implements the symbol table
 */
public class VarNames {
    private ArrayList<Symbol> list;

    public VarNames() {
        list = new ArrayList<>();
    }

    /**
     * Inserts a new symbol in the symbol table in lexicographical order at its first occurrence
     *
     * @param symbol to insert
     */
    public void addSymbol(Symbol symbol) {
        if (!isIn(symbol)) {
            list.add(symbol);
        }
    }

    /**
     * Checks if it's the first occurrence of a variable
     *
     * @param symbol the symbol we want to insert in the symbol table
     * @return true if symbol already exists in the symbol table and false if not
     */
    private boolean isIn(Symbol symbol) {
        for (int i = 0; i < list.size(); i++) {
            if (symbol.getValue().equals(list.get(i).getValue())){
                return true;
            }
        }
        return false;
    }

    /**
     * Display the content of the symbol table
     */
    public void printSymbolTable(){
    	Collections.sort(list, (o1, o2) -> o1.getValue().toString().compareTo(o2.getValue().toString()));
        System.out.println("\nVariables");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getValue() + " " + list.get(i).getLine());
        }
    }
}