import java.util.ArrayList;
import java.util.List;

/**
 * Build an abstract syntax tree from a parse tree
 */

public class AbstractSyntaxTree {
    private Symbol label; //value of the root
    private List<AbstractSyntaxTree> children; // Its children, which are trees themselves

    /**
     * Constructor
     *
     * @param label of the root of the tree
     * @param children list of children of the root
     */
    public AbstractSyntaxTree(Symbol label, List<AbstractSyntaxTree> children) {
        this.label = label;
        this.children = children;
    }

    /**
     * Constructor for a childless tree
     *
     * @param label of the root of the tree
     */
    public AbstractSyntaxTree(Symbol label) {
        this(label,new ArrayList<>());
    }

    /**
     * Constructor which takes a parse tree as input
     *
     * @param parseTree to convert to AST
     */
    public AbstractSyntaxTree(ParseTree parseTree) {
        this(parseTree.getLabel(), new ArrayList<>()); //set root label
        children = buildAST(parseTree.getChildren());
    }


    /**
     * Getter for the label attribute
     *
     * @return label of the root
     */
    public Symbol getLabel() {
        return label;
    }

    /**
     * Getter for the children attribute
     *
     * @return children of the tree
     */
    public List<AbstractSyntaxTree> getChildren() {
        return children;
    }

    /**
     * Computes a list of AST build from a list of Parse Trees
     * @param parseTree list of parse trees
     * @return list of AST
     */
    private List<AbstractSyntaxTree> buildAST(List<ParseTree> parseTree) {
        List<AbstractSyntaxTree> AST = new ArrayList<>();
        for (ParseTree child : parseTree) {
            if (child != null){
            
            int relevant = isRelevant(child); //check what type of node it is
            
            
            switch (relevant) {
                case -2: //non-terminal non relevant
                    List<AbstractSyntaxTree> children = buildAST(child.getChildren()); //we ignore the node and check the children
                    if (children.get(0) != null) {
                        AST.add(children.get(0)); //there will never be several children so we take the first
                    }
                    break;
                case -1: //terminal non relevant are just ignored
                    break;
                case 0: //exprArith
                    AbstractSyntaxTree exprArith = new AbstractSyntaxTree(child.getLabel());
                    exprArith.children.add(exprArith(child.getChildren())); //there is only one child in an arithmetical expression of an AST
                    
                    AST.add(exprArith);
                    break;
                case 1: //terminal relevant
                    AST.add(new AbstractSyntaxTree(child.getLabel()));
                    break;
                case 2: //non-terminal relevant
                    AST.add(new AbstractSyntaxTree(child.getLabel(), buildAST(child.getChildren())));
                    break;
                case 3:
                    List<AbstractSyntaxTree> childrenOfCond = new ArrayList<>();
                    childrenOfCond.add((exprArith(child.getChildren().get(0).getChildren())));
                    childrenOfCond.add((exprArith(child.getChildren().get(2).getChildren())));
                    AST.add(new AbstractSyntaxTree(child.getChildren().get(1).getChildren().get(0).getLabel(), childrenOfCond));
            }
            }
        }
        return AST;
    }

    /**
     * Build the AST of an arithmetic expression
     *
     * @param parseTree list of parse trees constituting an arithmetic expression
     * @return an abstract syntax tree of the expression
     */
    private AbstractSyntaxTree exprArith(List<ParseTree> parseTree) {
        AbstractSyntaxTree leftOperand = product(parseTree.get(0).getChildren());
        ParseTree recursiveExpr = parseTree.get(1);
        if (recursiveExpr != null) {
            AbstractSyntaxTree exprArith = recursiveExpression(parseTree.get(1).getChildren());
            AbstractSyntaxTree node = exprArith; //temporary node
            while(node.children.size() == 2) {
                node = node.children.get(0);
            }
            node.children.add(0,leftOperand);
            return exprArith;
        } else {
            return leftOperand;
        }
    }

    /**
     * Recursive function which turns the recursive part of an arithmetic expression into AST
     *
     * @param parseTree list of parse trees constituting a recursive arithmetic expression
     * @return an abstract syntax tree of the recursive expression
     */
    private AbstractSyntaxTree recursiveExpression(List<ParseTree> parseTree) {
        //PREPROCESSING
        AbstractSyntaxTree operator = new AbstractSyntaxTree(parseTree.get(0).getLabel()); //get operator
        AbstractSyntaxTree rightOperand;
        ParseTree rightChild = parseTree.get(1);
        if (rightChild.getLabel().getValue().toString() == "<Atom>") {
            rightOperand = atom(rightChild.getChildren());
        } else {
            rightOperand = product(rightChild.getChildren());
        }
        operator.children.add(rightOperand);
        System.out.println(rightOperand.getLabel());
        //explore the recursion to attach the current operation on the right place

        //RECURSION
        if (parseTree.get(2) == null) { //end of the recursion
            return operator; //the current operation is returned
        } else {
            AbstractSyntaxTree recursiveExpression = recursiveExpression(parseTree.get(2).getChildren());
            //POSTPROCESSING
            AbstractSyntaxTree node = recursiveExpression; //temporary node
            while(node.children.size() == 2) {
                node = node.children.get(0);
            }
            node.children.add(0,operator);
            return recursiveExpression;
        }
    }

    /**
     * Indirect recursive function which builds the AST of the operand of an arithmetic expression
     *
     * @param parseTree list of parse trees constituting the operand of an arithmetic expression
     * @return an abstract syntax tree of the operand
     */
    private AbstractSyntaxTree product(List<ParseTree> parseTree) {
        AbstractSyntaxTree rightOperand = null;
        AbstractSyntaxTree atom = atom(parseTree.get(0).getChildren());
        if (parseTree.get(1) != null) {
            rightOperand = recursiveExpression(parseTree.get(1).getChildren());
            AbstractSyntaxTree node = rightOperand; //temporary node
            while(node.children.size() == 2) {
                node = node.children.get(0);
            }
            node.children.add(0,atom);
            return rightOperand;
        } else {
            return atom;
        }

    }

    /**
     * Build the AST of an atom
     *
     * @param parseTree list of parse trees constituting the atom
     * @return an abstract syntax tree of the atom
     */
    private AbstractSyntaxTree atom(List<ParseTree> parseTree) {
        AbstractSyntaxTree atom = null;
        int nbrOfChildren = parseTree.size();
        if(nbrOfChildren == 1){
            atom = new AbstractSyntaxTree(parseTree.get(0).getLabel());
        }
        else if(nbrOfChildren == 2){
            atom = new AbstractSyntaxTree(parseTree.get(0).getLabel());
            atom.getChildren().add(atom(parseTree.get(1).getChildren()));
        }
        else if(nbrOfChildren == 3){
            atom = exprArith(parseTree.get(1).getChildren());
        }
        return atom;
    }

    /**
     * Checks if a given node is a terminal or non-terminal and whether it is relevant or not in the AST.
     * A specific treatment is reserved for arithmetical expressions and conditions.
     *
     * @param node to check
     * @return -2 if terminal not relevant, -1 if non-terminal not relevant, 0 if we enter an arithmetic expression,
     *          1 if terminal relevant, 2 if non-terminal relevant, 3 if we enter a condition
     */
    private int isRelevant(ParseTree node) {
        int ret;
        Symbol label = node.getLabel();
        if (label.isTerminal()) {
            switch (label.getType()) {
                case VARNAME:
                    ret = 1;
                    break;
                case NUMBER:
                    ret = 1;
                    break;
                case MINUS:
                    ret = 1;
                    break;
                case PLUS:
                    ret = 1;
                    break;
                case TIMES:
                    ret = 1;
                    break;
                case DIVIDE:
                    ret = 1;
                    break;
                case EQ:
                    ret = 1;
                    break;
                case GT:
                    ret = 1;
                    break;
                default:
                    ret = -1;
            }
        } else {
            switch (label.getValue().toString()) {
                case "<Program>":
                    ret = 2;
                    break;
                case "<Assign>":
                    ret = 2;
                    break;
                case "<If>":
                    ret = 2;
                    break;
                case "<While>":
                    ret = 2;
                    break;
                case "<Print>":
                    ret = 2;
                    break;
                case "<Read>":
                    ret = 2;
                    break;
                case "<Cond>":
                    ret = 3;
                    break;
                case "<Code>":
                    if (node.getChildren().get(0) != null) {
                        ret = 2;
                    } else {
                        ret = -1;
                    }
                    break;
                case "<ExprArith>":
                    ret = 0;
                    break;
                default:
                    ret = -2;
            }
        }
        return ret;
    }


    /**
     * Writes the tree as LaTeX code
     */
    public String toLaTexTree() {
        StringBuilder treeTeX = new StringBuilder();
        treeTeX.append("[");
        treeTeX.append("{" + label.toTexString() + "}");
        treeTeX.append(" ");

        for (AbstractSyntaxTree child : children) {
            if (child != null){
                treeTeX.append(child.toLaTexTree());
            }
        }
        treeTeX.append("]");
        return treeTeX.toString();
    }

    /**
     * Writes the tree as TikZ code. TikZ is a language to specify drawings in LaTeX
     * files.
     */
    public String toTikZ() {
        StringBuilder treeTikZ = new StringBuilder();
        treeTikZ.append("node {");
        treeTikZ.append(label.toTexString());
        treeTikZ.append("}\n");
        for (AbstractSyntaxTree child : children) {
            if (child != null){
                treeTikZ.append("child { ");
                treeTikZ.append(child.toTikZ());
                treeTikZ.append(" }\n");
            }
        }
        return treeTikZ.toString();
    }

    /**
     * Writes the tree as a TikZ picture. A TikZ picture embeds TikZ code so that
     * LaTeX undertands it.
     */
    public String toTikZPicture() {
        return "\\begin{tikzpicture}[tree layout]\n\\" + toTikZ() + ";\n\\end{tikzpicture}";
    }

    /**
     * Writes the tree as a LaTeX document which can be compiled (using the LuaLaTeX
     * engine). Be careful that such code will not compile with PDFLaTeX, since the
     * tree drawing algorithm is written in Lua. The code is not very readable as
     * such, but you can have a look at the outputted file if you want to understand
     * better. <br>
     * <br>
     * The result can be used with the command:
     * 
     * <pre>
     * lualatex some-file.tex
     * </pre>
     */
    public String toLaTeXLua() {
        return "\\RequirePackage{luatex85}\n\\documentclass{standalone}\n\n\\usepackage[T1]{fontenc}\n\\usepackage{tikz}\n\n\\usetikzlibrary{graphdrawing, graphdrawing.trees}\n\n\\begin{document}\n\n"
                + toTikZPicture() + "\n\n\\end{document}\n%% Local Variables:\n%% TeX-engine: luatex\n%% End:";
    }

    /**
     * Writes the tree as a forest picture. Returns the tree in forest enviroment
     * using the latex code of the tree
     */
    public String toForestPicture() {
        return "\\begin{forest}for tree={rectangle, draw, l sep=20pt}" + toLaTexTree() + ";\n\\end{forest}";
    }

    /**
     * Writes the tree as a LaTeX document which can be compiled using PDFLaTeX.
     * <br>
     * <br>
     * The result can be used with the command:
     * 
     * <pre>
     * pdflatex some-file.tex
     * </pre>
     */
    public String toLaTeX() {
        return "\\documentclass[border=5pt]{standalone}\n\n\\usepackage[T1]{fontenc}\n\\usepackage{tikz}\n\\usepackage{forest}\n\n\\begin{document}\n\n"
                + toForestPicture() + "\n\n\\end{document}\n%% Local Variables:\n%% TeX-engine: pdflatex\n%% End:";
    }
}