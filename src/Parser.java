import java.util.ArrayList;
import java.util.List;


class Parser{

    private List<Symbol> tokens;
    boolean verbose;
    private int pos;

    /**
     * Initiates the parser to position 0
     *
     * @param tokens sequence of Symbols to parse
     * @param verbose display flag
     */
    Parser(List<Symbol> tokens, boolean verbose){
        this.tokens = tokens;
        this.verbose = verbose;
        this.pos = 0;
    }

    /**
     * Starts the parsing
     *
     * @return parse tree
     * @throws SyntaxError
     */
    public ParseTree parse()throws SyntaxError{
        return parseProgram();
    }
    /**
     * Process the parsing of the program based on the first rule of the grammar
     *
     * @return parse tree of the program
     * @throws SyntaxError
     */
    private ParseTree parseProgram()throws SyntaxError{
        displayRule(1, "<Program> --> BEGINPROG [ProgName] [EndLine] <Code> ENDPROG");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(lookAhead(LexicalUnit.BEGINPROG));
        children.add(lookAhead(LexicalUnit.PROGNAME));
        children.add(lookAhead(LexicalUnit.ENDLINE));
        children.add(parseCode());
        children.add(lookAhead(LexicalUnit.ENDPROG));

        return new ParseTree(new Symbol(null, "<Program>"), children);
    }

    /**
     * Process the parsing of a code section
     *
     * @return parse tree of the code section
     * @throws SyntaxError
     */
    private ParseTree parseCode()throws SyntaxError{
        if (
                tokens.get(pos).getType() == LexicalUnit.VARNAME ||
                tokens.get(pos).getType() == LexicalUnit.IF ||
                tokens.get(pos).getType() == LexicalUnit.WHILE ||
                tokens.get(pos).getType() == LexicalUnit.PRINT ||
                tokens.get(pos).getType() == LexicalUnit.READ)
            {
                displayRule(2, "<Code> --> <Instruction> [EndLine] <Code>");
                List<ParseTree> children = new ArrayList<ParseTree>();
                children.add(parseInstruction());
                children.add(lookAhead(LexicalUnit.ENDLINE));
                children.add(parseCode());
                return new ParseTree(new Symbol(null, "<Code>"), children);
            }
        else if (
                tokens.get(pos).getType() == LexicalUnit.ENDPROG ||
                tokens.get(pos).getType() == LexicalUnit.ENDIF ||
                tokens.get(pos).getType() == LexicalUnit.ELSE ||
                tokens.get(pos).getType() == LexicalUnit.ENDWHILE){
                displayRule(3, "<Code> --> Epsilon ");
                return null;
                }
        throw new SyntaxError(tokens.get(pos).toString() + " NOT EXPECTED TOKEN");
    }

    /**
     * Process the parsing of an instruction
     *
     * @return parse tree of the instruction
     * @throws SyntaxError
     */
    private ParseTree parseInstruction()throws SyntaxError{
        List<ParseTree> children = new ArrayList<ParseTree>();
        if(tokens.get(pos).getType() == LexicalUnit.VARNAME){
            displayRule(4, "<Instruction> --> <Assign>");
            children.add(parseAssign());
            return new ParseTree(new Symbol(null, "<Instruction>"), children);
        }
        else if(tokens.get(pos).getType() == LexicalUnit.IF){
            displayRule(5, "<Instruction> --> <If>");
            children.add(parseIf());
            return new ParseTree(new Symbol(null, "<Instruction>"), children);
        }
        else if(tokens.get(pos).getType() == LexicalUnit.WHILE){
            displayRule(6, "<Instruction> --> <While>");
            children.add(parseWhile());
            return new ParseTree(new Symbol(null, "<Instruction>"), children);
        }
        else if(tokens.get(pos).getType() == LexicalUnit.PRINT){
            displayRule(7, "<Instruction> --> <Print>");
            children.add(parsePrint());
            return new ParseTree(new Symbol(null, "<Instruction>"), children);
        }
        else {
            displayRule(8, "<Instruction> --> <Read>");
            children.add(parseRead());
            return new ParseTree(new Symbol(null, "<Instruction>"), children);
        }
    }

    /**
     * Process the parsing of an assign instruction
     *
     * @return parse tree of the assign instruction
     * @throws SyntaxError
     */
    private ParseTree parseAssign()throws SyntaxError{
        displayRule(9, "<Assign> --> [VarName] := <ExprArith>");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(lookAhead(LexicalUnit.VARNAME));
        children.add(lookAhead(LexicalUnit.ASSIGN));
        children.add(parseExprArith());

        return new ParseTree(new Symbol(null, "<Assign>"), children);
    }

    /**
     * Process the parsing of an arithmetic expression
     *
     * @return parse tree of the arithmetic expression
     * @throws SyntaxError
     */
    private ParseTree parseExprArith()throws SyntaxError{
        displayRule(10, "<ExprArith> --> <Product><ExprArith'>");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(parseProduct());
        children.add(parseExprArithPrime());

        return new ParseTree(new Symbol(null, "<ExprArith>"), children);
    }

    /**
     * Process the parsing of an arithmetic expression prime
     *
     * @return parse tree of the arithmetic expression prime
     * @throws SyntaxError
     */
    private ParseTree parseExprArithPrime()throws SyntaxError{
        List<ParseTree> children = new ArrayList<ParseTree>();
        if(tokens.get(pos).getType() == LexicalUnit.PLUS){
            displayRule(11, "<ExprArith'> --> +<Product><ExprArith'>");
            children.add(lookAhead(LexicalUnit.PLUS));
            children.add(parseProduct());
            children.add(parseExprArithPrime());
            return new ParseTree(new Symbol(null, "<ExprArith'>"), children);
        }
        else if(tokens.get(pos).getType() == LexicalUnit.MINUS){
            displayRule(12, "<ExprArith'> --> -<Product><ExprArith'>");
            children.add(lookAhead(LexicalUnit.MINUS));
            children.add(parseProduct());
            children.add(parseExprArithPrime());
            return new ParseTree(new Symbol(null, "<ExprArith'>"), children);
        }
        else if (
            tokens.get(pos).getType() == LexicalUnit.ENDLINE ||
            tokens.get(pos).getType() == LexicalUnit.DO ||
            tokens.get(pos).getType() == LexicalUnit.THEN ||
            tokens.get(pos).getType() == LexicalUnit.EQ ||
            tokens.get(pos).getType() == LexicalUnit.GT||
            tokens.get(pos).getType() == LexicalUnit.RPAREN){
                displayRule(13, "<ExprArithPrime> --> Epsilon");
                return null;
            }
        throw new SyntaxError(tokens.get(pos).toString() + " NOT EXPECTED TOKEN");
    }

    /**
     * Process the parsing of a product
     *
     * @return parse tree of the product
     * @throws SyntaxError
     */
    private ParseTree parseProduct()throws SyntaxError{
        displayRule(14, "<Product> --> <Atom><ProductPrime>");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(parseAtom());
        children.add(parseProductPrime());
        return new ParseTree(new Symbol(null, "<Product>"), children);
    }

    /**
     * Process the parsing of a product prime
     *
     * @return parse tree of the product prime
     * @throws SyntaxError
     */
    private ParseTree parseProductPrime()throws SyntaxError{
        List<ParseTree> children = new ArrayList<ParseTree>();
        if(tokens.get(pos).getType() == LexicalUnit.TIMES){
            displayRule(15, "<ProductPrime> --> *<Atom><ProductPrime>");
            children.add(lookAhead(LexicalUnit.TIMES));
            children.add(parseAtom());
            children.add(parseProductPrime());
            return new ParseTree(new Symbol(null, "<ProductPime>"), children);
        }
        else if(tokens.get(pos).getType() == LexicalUnit.DIVIDE){
            displayRule(16, "<ProductPrime> --> /<Atom><ProductPrime>");
            children.add(lookAhead(LexicalUnit.DIVIDE));
            children.add(parseAtom());
            children.add(parseProductPrime());
            return new ParseTree(new Symbol(null, "<ProductPrime>"), children);
        }
        else if (
            tokens.get(pos).getType() == LexicalUnit.ENDLINE ||
            tokens.get(pos).getType() == LexicalUnit.DO ||
            tokens.get(pos).getType() == LexicalUnit.THEN ||
            tokens.get(pos).getType() == LexicalUnit.EQ ||
            tokens.get(pos).getType() == LexicalUnit.GT ||
            tokens.get(pos).getType() == LexicalUnit.PLUS ||
            tokens.get(pos).getType() == LexicalUnit.MINUS ||
            tokens.get(pos).getType() == LexicalUnit.RPAREN){
                displayRule(17, "<ProductPrime> --> Epsilon");
                return null;
            }
        throw new SyntaxError(tokens.get(pos).toString() + " NOT EXPECTED TOKEN");
    }

    /**
     * Process the parsing of an atom
     *
     * @return parse tree of the atom
     * @throws SyntaxError
     */
    private ParseTree parseAtom()throws SyntaxError{
        List<ParseTree> children = new ArrayList<ParseTree>();
        if(tokens.get(pos).getType() == LexicalUnit.VARNAME){
            displayRule(18, "<Atom> --> [VarName]");
            children.add(lookAhead(LexicalUnit.VARNAME));

            return new ParseTree(new Symbol(null, "<Atom>"), children);
        }
        else if(tokens.get(pos).getType() == LexicalUnit.NUMBER){
            displayRule(19, "<Atom> --> [Number]");
            children.add(lookAhead(LexicalUnit.NUMBER));

            return new ParseTree(new Symbol(null, "<Atom>"), children);
        }
        else if(tokens.get(pos).getType() == LexicalUnit.LPAREN){
            displayRule(20, "<Atom> --> (<ExprArith>)");
            children.add(lookAhead(LexicalUnit.LPAREN));
            children.add(parseExprArith());
            children.add(lookAhead(LexicalUnit.RPAREN));

            return new ParseTree(new Symbol(null, "<Atom>"), children);

        }
        else{
            displayRule(21, "<Atom> --> -<Atom>");
            children.add(lookAhead(LexicalUnit.MINUS));
            children.add(parseAtom());

            return new ParseTree(new Symbol(null, "<Atom>"), children);
        }
    }
    /**
     * Process the parsing of an If instruction
     *
     * @return parse tree of the If instruction
     * @throws SyntaxError
     */
    private ParseTree parseIf() throws SyntaxError{
        displayRule(22, "<If> --> IF (<Cond>) THEN [EndLine] <Code> <IfTail>");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(lookAhead(LexicalUnit.IF));
        children.add(lookAhead(LexicalUnit.LPAREN));
        children.add(parseCond());
        children.add(lookAhead(LexicalUnit.RPAREN));
        children.add(lookAhead(LexicalUnit.THEN));
        children.add(lookAhead(LexicalUnit.ENDLINE));
        children.add(parseCode());
        children.add(parseIfTail());

        return new ParseTree(new Symbol(null, "<If>"), children);
    }

    /**
     * Process the parsing of an If tail
     *
     * @return parse tree of the If tail
     * @throws SyntaxError
     */
    private ParseTree parseIfTail() throws SyntaxError{
        List<ParseTree> children = new ArrayList<ParseTree>();
        if(tokens.get(pos).getType() == LexicalUnit.ENDIF){
            displayRule(23, "<IfTail> --> ENDIF");
            children.add(lookAhead(LexicalUnit.ENDIF));

            return new ParseTree(new Symbol(null, "<IfTail>"), children);
        }
        else{
            displayRule(24, "<IfTail> --> ELSE [EndLine] <Code> ENDIF");
            children.add(lookAhead(LexicalUnit.ELSE));
            children.add(lookAhead(LexicalUnit.ENDLINE));
            children.add(parseCode());
            children.add(lookAhead(LexicalUnit.ENDIF));

            return new ParseTree(new Symbol(null, "<IfTail>"), children);
        }
    }

    /**
     * Process the parsing of a condition
     *
     * @return parse tree of the condition
     * @throws SyntaxError
     */
    private ParseTree parseCond() throws SyntaxError{
        displayRule(25, "<Cond> --> <ExprArith><Comp><ExprArith>");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(parseExprArith());
        children.add(parseComp());
        children.add(parseExprArith());

        return new ParseTree(new Symbol(null, "<Cond>"), children);
    }

    /**
     * Process the parsing of a comparison
     *
     * @return parse tree of the comparison
     * @throws SyntaxError
     */
    private ParseTree parseComp() throws SyntaxError{
        displayRule(26, "<Comp>-->=");
        List<ParseTree> children = new ArrayList<ParseTree>();
        if(tokens.get(pos).getType() == LexicalUnit.EQ){
            children.add(lookAhead(LexicalUnit.EQ));

            return new ParseTree(new Symbol(null, "<Comp>"), children);
        }
        else{
            displayRule(27, "<Comp>-->>");
            children.add(lookAhead(LexicalUnit.GT));

            return new ParseTree(new Symbol(null, "<Comp>"), children);
        }
    }

    /**
     * Process the parsing of a While instruction
     *
     * @return parse tree of the While instruction
     * @throws SyntaxError
     */
    private ParseTree parseWhile() throws SyntaxError{
        displayRule(28, "<While>--> WHILE (<Cond>) DO [EndLine] <Code> ENDWHILE ");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(lookAhead(LexicalUnit.WHILE));
        children.add(lookAhead(LexicalUnit.LPAREN));
        children.add(parseCond());
        children.add(lookAhead(LexicalUnit.RPAREN));
        children.add(lookAhead(LexicalUnit.DO));
        children.add(lookAhead(LexicalUnit.ENDLINE));
        children.add(parseCode());
        children.add(lookAhead(LexicalUnit.ENDWHILE));

        return new ParseTree(new Symbol(null, "<While>"), children);
    }

    /**
     * Process the parsing of a print instruction
     *
     * @return parse tree of the print instruction
     * @throws SyntaxError
     */
    private ParseTree parsePrint() throws SyntaxError{
        displayRule(29, "<Print> --> PRINT([VarName])");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(lookAhead(LexicalUnit.PRINT));
        children.add(lookAhead(LexicalUnit.LPAREN));
        children.add(lookAhead(LexicalUnit.VARNAME));
        children.add(lookAhead(LexicalUnit.RPAREN));

        return new ParseTree(new Symbol(null, "<Print>"), children);
    }

    /**
     * Process the parsing of a read instruction
     *
     * @return parse tree of the read instruction
     * @throws SyntaxError
     */
    private ParseTree parseRead() throws SyntaxError{
        displayRule(30, " <Read>--> READ([VarName])");
        List<ParseTree> children = new ArrayList<ParseTree>();
        children.add(lookAhead(LexicalUnit.READ));
        children.add(lookAhead(LexicalUnit.LPAREN));
        children.add(lookAhead(LexicalUnit.VARNAME));
        children.add(lookAhead(LexicalUnit.RPAREN));

        return new ParseTree(new Symbol(null, "<Read>"), children);
    }

    /**
     * Checks that the look-ahead has the expected type and throws a syntax error if not
     *
     * @param type expected type of the look-ahead
     * @return parse tree of the terminal
     * @throws SyntaxError
     */
    private ParseTree lookAhead(LexicalUnit type) throws SyntaxError{
        Symbol terminal = tokens.get(pos++);
        if(terminal.getType() == type){
            return new ParseTree(terminal);
        }
        else{
            throw new SyntaxError(tokens.get(pos-1).toString() + " NOT EXPECTED TOKEN, EXPECTED TOKEN :" + type);
        }
    }

    private void displayRule(int numRule, String rule){
        if(verbose){
            System.out.println("Rule number: "+ numRule + ", rule: "+rule);
        }
        else{
            System.out.println("Rule number: "+ numRule);
        }
    }
}