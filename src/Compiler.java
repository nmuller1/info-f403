import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
public class Compiler{
    private String fileContent;
    private int unnamedTemporaries;
    private int whileLabelNum;
    private int ifLabelNum;
    private List<String> identifiersList;
    private HashMap<LexicalUnit, String> operands = new HashMap<LexicalUnit, String>();

    public Compiler(){
        operands.put(LexicalUnit.MINUS, "sub i32");
        operands.put(LexicalUnit.PLUS, "add i32");
        operands.put(LexicalUnit.TIMES, "mul i32");
        operands.put(LexicalUnit.DIVIDE, "sdiv i32");
        operands.put(LexicalUnit.EQ, "icmp eq i32");
        operands.put(LexicalUnit.GT, "icmp sgt i32");
        fileContent = "\n";
        unnamedTemporaries = 0;
        whileLabelNum = 0;
        ifLabelNum = 0;
        identifiersList = new ArrayList<>();
    }

    /**
     * Entry point of the compilation
     *
     * @param AST for which we are going to generate the LLVM code
     * @return LLVM output code
     */
    public String compile(AbstractSyntaxTree AST){
        compileProgram(AST);
        return fileContent;
    }

    /**
     * Generate the LLVM code corresponding to the provided AST
     *
     * @param AST for which we are going to generate the LLVM code
     */
    private void compileProgram(AbstractSyntaxTree AST){
        fileContent += "@.strR = private unnamed_addr constant [3 x i8] c\"%d\00\", align 1\n";
        defineRead();
        definePrintln();
        fileContent += "define i32 @main() {\n";
        fileContent += "\tentry:\n";
        compileCode(AST.getChildren().get(0));
        fileContent += "ret i32 0\n";
        fileContent += "}";
    }

    /**
     * Add the LLVM code for the read function
     */
    private void defineRead(){
        fileContent += "define i32 @readInt() #0 {\n";
        fileContent += "\t%1 = alloca i32, align 4\n";
        fileContent += "\t%2 = call i32 (i8*, ...) @scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.strR, i32 0, i32 0), i32* %1)\n";
        fileContent += "\t%3 = load i32, i32* %1, align 4\n";
        fileContent += "\tret i32 %3\n";
        fileContent += "}\n";
        fileContent += "declare i32 @scanf(i8*, ...) #1\n";
    }

    /**
     * Add the LLVM code for the print function
     */
    private void definePrintln(){
        fileContent += "define void @println(i32 %x) #0 {\n";
        fileContent += "\t%1 = alloca i32, align 4\n";
        fileContent += "\tstore i32 %x, i32* %1, align 4\n";
        fileContent += "\t%2 = load i32, i32* %1, align 4\n";
        fileContent += "\t%3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.strP, i32 0, i32 0), i32 %2)\n";
        fileContent += "\tret void\n";
        fileContent += "}\n";
        fileContent += "declare i32 @printf(i8*, ...) #1\n";
    }

    /**
     * Explore the provided AST while generating the corresponding LLVM code
     *
     * @param AST to explore
     */
    private void compileCode(AbstractSyntaxTree AST){
        for(AbstractSyntaxTree child : AST.getChildren()){
            String instruction = child.getLabel().getValue().toString();
            if (instruction.equals("<Assign>")){
                compileAssign(child);
            }
            else if (instruction.equals("<If>")){
                compileIf(child);
            }
            else if (instruction.equals("<While>")){
                compileWhile(child);
            }
            else if (instruction.equals("<Print>")){
                compilePrint(child);
            }
            else if (instruction.equals("<Read>")){
                compileRead(child);
            }
            else if (instruction.equals("<Code>")){
                compileCode(child);
            }
        }
    }

    /**
     * Generate the LLVM code corresponding to the assign instruction
     *
     * @param AST of the assign instruction
     */
    private void compileAssign(AbstractSyntaxTree AST){
        String identifier = AST.getChildren().get(0).getLabel().getValue().toString();
        if(identifiersList.contains(identifier)){
            identifiersList.add(identifier);
            fileContent += "\t%" + identifier + " = alloca i32\n";
        }
        AbstractSyntaxTree value = AST.getChildren().get(1);  
        if(value.getLabel().getType() == LexicalUnit.NUMBER){
            fileContent += "\tstore i32 " + (int)value.getLabel().getValue() + ", i32* %"+identifier+"\n";
        }
        else{
            compileOperands(value.getChildren().get(0));
            fileContent += "\tstore i32 %" + unnamedTemporaries + ", i32* %"+identifier+"\n";
            unnamedTemporaries++;

        }

    }

    /**
     * Generate the LLVM code corresponding to the If instruction
     *
     * @param AST of the If instruction
     */
    private void compileIf(AbstractSyntaxTree AST){
        compileOperands(AST.getChildren().get(0));
        boolean isElse = AST.getChildren().size()>2;
        if(!isElse){
            fileContent += "\tbr i1 %" + unnamedTemporaries + ", label %ifLabel" + ifLabelNum + ", label %endIfLabel" + ifLabelNum +"\n";
        }
        else{
            fileContent += "\tbr i1 %" + unnamedTemporaries + ", label %ifLabel" + ifLabelNum + ", label %elseLabel" + ifLabelNum + "\n";
        }
        fileContent += "\tifLabel" + ifLabelNum +":\n";
        compileCode(AST.getChildren().get(1));
        fileContent += "\tbr label %endIfLabel" + ifLabelNum + "\n";
        if(isElse){
            fileContent += "\telseLabel" + ifLabelNum + ":\n";
            compileCode(AST.getChildren().get(1));
            fileContent += "\tbr label %endIfLabel" + ifLabelNum + "\n";
        }
        fileContent += "\tendIfLabel"+ifLabelNum+":\n";

        ifLabelNum++;
        unnamedTemporaries++;
    }

    /**
     * Generate the LLVM code corresponding to the While instruction
     *
     * @param AST of the While instruction
     */
    private void compileWhile(AbstractSyntaxTree AST){
        fileContent += "\tbr label %whileCondLabel"+whileLabelNum+"\n";
        fileContent += "\twhileCondLabel"+whileLabelNum+":\n";
        compileOperands(AST.getChildren().get(0));
        fileContent += "\tbr i1 %" + unnamedTemporaries + ", label %twhileLabel" + whileLabelNum + ", label %endwhileLabel" + whileLabelNum+"\n";
        fileContent += "\twhileLabel" + whileLabelNum + ":\n";
        compileCode(AST.getChildren().get(1));
        fileContent += "\tbr label %whileCondLabel" + whileLabelNum + "\n";
        fileContent += "\tendwhileLabel" + whileLabelNum + ":\n";
        whileLabelNum++;
        unnamedTemporaries++;
    }

    /**
     * Generate the LLVM code corresponding to the Print instruction
     *
     * @param AST of the Print instruction
     */
    private void compilePrint(AbstractSyntaxTree AST){
        String identifier = AST.getChildren().get(0).getLabel().getValue().toString();
        fileContent += "\t%"+unnamedTemporaries+" = load i32, i32* %"+identifier+"\n";
        fileContent += "\tcall void @println(i32 %" + unnamedTemporaries + ")\n";
        unnamedTemporaries++;
    }

    /**
     * Generate the LLVM code corresponding to the Read instruction
     *
     * @param AST of the Read instruction
     */
    private void compileRead(AbstractSyntaxTree AST){
        String identifier = AST.getChildren().get(0).getLabel().getValue().toString();
        if(identifiersList.contains(identifier)){
            identifiersList.add(identifier);
            fileContent += "\t%" + identifier + " = alloca i32\n";
        }
        fileContent += "\t%" + unnamedTemporaries + " = call i32 @readInt()\n";
        fileContent += "\tstore i32 %" + unnamedTemporaries + ", i32* %" + identifier + "\n";
        unnamedTemporaries++;
    }

    /**
     * Generate the LLVM code corresponding to an operation
     *
     * @param AST of the operation
     */
    private void compileOperands(AbstractSyntaxTree AST){

        if(AST.getChildren().size() == 0){
            fileContent += "\t%" + unnamedTemporaries + " = load i32, i32* %" + AST.getLabel().getValue() + "\n";
        }
        else if(AST.getChildren().size() == 1){
            fileContent += "\t%" +unnamedTemporaries+" = add i32 -1, 0\n";
            unnamedTemporaries++;
            fileContent += "\t%"+unnamedTemporaries+" = add i32"+ AST.getChildren().get(0).getLabel().getValue().toString() +", 0\n";
            unnamedTemporaries++;
            fileContent += "\t%"+unnamedTemporaries+" = mul i32 %"+ Integer.toString(unnamedTemporaries-2) +", %"+Integer.toString(unnamedTemporaries-1)+"\n";
        }
        else {
            Symbol operand = AST.getLabel();
            AbstractSyntaxTree leftTerm = AST.getChildren().get(0);
            String leftTermValue;
            if(leftTerm.getLabel().getType() != LexicalUnit.NUMBER){
                compileOperands(leftTerm);
                leftTermValue = "%"+unnamedTemporaries;
                unnamedTemporaries++;
            }
            else{
                leftTermValue = leftTerm.getLabel().getValue().toString();
            }
            AbstractSyntaxTree rightChild = AST.getChildren().get(1);
            String rightTermInt;
            if(rightChild.getLabel().getType() != LexicalUnit.NUMBER){
                compileOperands(rightChild);
                rightTermInt = "%"+unnamedTemporaries;
                unnamedTemporaries++;
            }
            else{
                rightTermInt = rightChild.getLabel().getValue().toString();
            }
            
            fileContent += "\t%" + unnamedTemporaries + " = " + operands.get(operand.getType()) + " "+ leftTermValue + ", "+rightTermInt+"\n";
        }
    }
}