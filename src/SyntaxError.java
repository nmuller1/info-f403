/**
 *  Exception raised when a unexpected token is met during the parsing
 */
class SyntaxError extends Exception{
    SyntaxError(String s){  
    super(s);  
    }  
}  